package org.instedd.resourcemap.sql;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * SQLite helper class
 * @author Sokha RUM
 */
class SQLiteHelper extends SQLiteOpenHelper{
	public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "resourcemap.db";
    public static final String CREATE_TABLE_SETTING = "CREATE TABLE rm_setting(rm_url VARCHAR(100) NOT NULL)";
    public static final String INITIALIZE_DATA = "INSERT INTO rm_setting (rm_url) VALUES ('http://resourcemap-sea.instedd.org')";
	public SQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_SETTING);
		db.execSQL(INITIALIZE_DATA);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS rm_setting");
 
        // Create tables again
        onCreate(db);
	}

	/**
	 * Getting resource map url in setting
	 * @return String url
	 */
	public String getReourceMapUrl() {
		String url = null;
		String sql = "SELECT rm_url FROM rm_setting";
		SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
        	url = cursor.getString(0);
        }
		return url;
	}

}
