package org.instedd.resourcemap;

import org.instedd.resourcemap.services.ResourceMapClient;
import org.instedd.resourcemap.services.ResourceMapServiceStatus;
import org.instedd.resourcemap.services.result.LoginResult;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		/*
		Button btnLogin = (Button) findViewById(R.id.btnLogin);
        
		btnLogin.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				finish();
			}
		});*/
		//ActionBar actionBar = getActionBar();
		//actionBar.hide();
	}

	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	*/
	public void login(View view) {
		EditText email = (EditText) findViewById(R.id.txt_email);
		EditText pwd = (EditText) findViewById(R.id.txt_pwd);
		
        ResourceMapClient client = ResourceMapClient.getInStance();
        
        LoginResult result = client.authentication(email.getText().toString(), pwd.getText().toString());
        
        String message = "";
        if (result.getStatus() == ResourceMapServiceStatus.SUCCESS) {
        	//message = result.getAuthToken();
        	Intent collection = new Intent(LoginActivity.this, CollectionActivity.class);
        	this.startActivity(collection);
        	
        } else {
        	message = result.getErrorMessage();
        	AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this, R.style.AlertDialogCustom);
    		//builder.setMessage(String.valueOf(success) + "/" + message).setTitle("Warning");
    		builder.setMessage(message).setTitle("Warning");
    		builder.setNegativeButton(" OK ", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	               // User cancelled the dialog
    	           }
    	       });

    		
    		AlertDialog dialog = builder.create();
    		dialog.show();

        }
        
	}

}
