package org.instedd.resourcemap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class HttpTesting {


	/** private aut_token*/ 
	private String autToken = "";
	
	/** the url of resource map */
	private String host = "http://resmap-stg-ilab.instedd.org";
	
	/** the timeout connection in milisecond*/
	private int timeOut = 10000;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) { 
		HttpClient httpclient = new DefaultHttpClient();
		String serviceUrl = "http://resmap-stg-ilab.instedd.org/api/users/sign_in";
	    HttpPost httppost = new HttpPost(serviceUrl);
	    
	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	        
	        nameValuePairs.add(new BasicNameValuePair("user[email]", "toto@toto.com"));
	        nameValuePairs.add(new BasicNameValuePair("user[password]", "123"));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        String responseBody = EntityUtils.toString(response.getEntity());
	        System.out.print(responseBody);
	        JSONObject json = new JSONObject();
	        

	    } catch (ClientProtocolException e) {
	        e.printStackTrace(); 
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	    

	}

}
