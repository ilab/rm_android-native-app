package org.instedd.resourcemap.services;

public class ResourceMapServiceStatus {
	public static final int SUCCESS = 1;
	public static final int FAILED = 0;
	public static final int ERROR = 2;
	
	protected int status = FAILED;
	protected String errorMessage = "";
	
	public int getStatus() {
		return this.status;
	}
	
	public String getErrorMessage() {
		return this.errorMessage;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
