package org.instedd.resourcemap.services;


import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.instedd.resourcemap.services.result.CollectionResult;
import org.instedd.resourcemap.services.result.LoginResult;
import org.instedd.resourcemap.vo.CollectionVO;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.webkit.URLUtil;

/**
 * The web service client for connection to resource map
 * @author sokha 
 */
public class ResourceMapClient {
	private static ResourceMapClient instance = null;
	
	/** private aut_token*/ 
	private String autToken = "";
	
	/** the url of resource map */
	private String host = "http://resmap-stg-ilab.instedd.org";
	
	/** the timeout connection in milisecond*/
	private int timeOut = 10000;
	
	private ResourceMapClient() {
		// private constructor
	}
	
	public static ResourceMapClient getInStance() {
		if (instance == null) {
			instance = new ResourceMapClient();
		}
		return instance;
	}

	public LoginResult authentication(String email, String pwd) {
		LoginResult result = new LoginResult();
		HttpParams httpParameters = new BasicHttpParams();
        int timeoutConnection = this.timeOut;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = this.timeOut;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		
		HttpClient httpclient = new DefaultHttpClient(httpParameters);
		String serviceUrl = this.host + "/api/users/sign_in";
		//serviceUrl = "http://resmap-stg-ilab.instedd.org/api/users/sign_in1";
		if (!URLUtil.isValidUrl(serviceUrl)) {
			result.setStatus(ResourceMapServiceStatus.FAILED);
			result.setErrorMessage("Cannot connect to server");
			return result;
		}
	    HttpPost httppost = new HttpPost(serviceUrl);
	    String responseBody = null;
	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	        
	        nameValuePairs.add(new BasicNameValuePair("user[email]", email));
	        nameValuePairs.add(new BasicNameValuePair("user[password]", pwd));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        httppost.addHeader("accept", "text/html");
	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        responseBody = EntityUtils.toString(response.getEntity());
	        httpclient.getConnectionManager().shutdown();

	    } catch (ClientProtocolException e) {
	        result.setErrorMessage(e.getMessage());
	        result.setStatus(ResourceMapServiceStatus.ERROR);
	    }  catch (IOException e) {
	    	result.setErrorMessage(e.getMessage());
	    	result.setStatus(ResourceMapServiceStatus.ERROR);
	    } 
	    
	    if (result.getStatus() != ResourceMapServiceStatus.ERROR && responseBody != "") {
	    	try { 
				JSONObject json = new JSONObject(responseBody);
				boolean success = json.getBoolean("success");
				if (success) {
					result.setStatus(ResourceMapServiceStatus.SUCCESS);
					result.setAuthToken(json.getString("auth_token"));
				} else {
					result.setStatus(ResourceMapServiceStatus.FAILED);
					result.setErrorMessage(json.getString("message"));
				}
				
				
			} catch (JSONException e) {
				result.setErrorMessage(e.getMessage());
		    	result.setStatus(ResourceMapServiceStatus.ERROR);
			}
	    	
	    }
	    return result;
		
	}
	
	public ResourceMapServiceStatus logout() {
		ResourceMapServiceStatus result = new ResourceMapServiceStatus();
		HttpParams httpParameters = new BasicHttpParams();
        int timeoutConnection = this.timeOut;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = this.timeOut;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		
        if (!URLUtil.isValidUrl(this.host)) {
			result.setStatus(ResourceMapServiceStatus.FAILED);
			result.setErrorMessage("Cannot connect to server");
			return result;
		}
        
		HttpClient httpclient = new DefaultHttpClient(httpParameters);
		String serviceUrl = this.host + "/api/users/sign_out?auth_token=" + this.autToken;
		//serviceUrl = "http://resmap-stg-ilab.instedd.org/api/users/sign_in1";
		
	    HttpPost httppost = new HttpPost(serviceUrl);
	    try {
	        
	        httpclient.execute(httppost);
	        httpclient.getConnectionManager().shutdown();

	    } catch (ClientProtocolException e) {
	        result.setErrorMessage(e.getMessage());
	        result.setStatus(ResourceMapServiceStatus.ERROR);
	    }  catch (IOException e) {
	    	result.setErrorMessage(e.getMessage());
	    	result.setStatus(ResourceMapServiceStatus.ERROR);
	    } 
	    
	    return result;
	    
	}
	
	public CollectionResult getCollection() {
		CollectionResult result = new CollectionResult();
		HttpParams httpParameters = new BasicHttpParams();
        int timeoutConnection = this.timeOut;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = this.timeOut;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		
		HttpClient httpclient = new DefaultHttpClient(httpParameters);
		String serviceUrl = this.host + "/api/collections?auth_token=" + this.autToken;
		//serviceUrl = "http://resmap-stg-ilab.instedd.org/api/users/sign_in1";
		if (!URLUtil.isValidUrl(serviceUrl)) {
			result.setStatus(ResourceMapServiceStatus.FAILED);
			result.setErrorMessage("Cannot connect to server");
			return result;
		}
	    HttpPost httppost = new HttpPost(serviceUrl);
	    String responseBody = null;
	    try {
	        // Add your data
	        //List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	        
	        //nameValuePairs.add(new BasicNameValuePair("user[email]", email));
	        //nameValuePairs.add(new BasicNameValuePair("user[password]", pwd));
	        //httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        httppost.addHeader("accept", "text/html");
	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        responseBody = EntityUtils.toString(response.getEntity());
	        httpclient.getConnectionManager().shutdown();

	    } catch (ClientProtocolException e) {
	        result.setErrorMessage(e.getMessage());
	        result.setStatus(ResourceMapServiceStatus.ERROR);
	    }  catch (IOException e) {
	    	result.setErrorMessage(e.getMessage());
	    	result.setStatus(ResourceMapServiceStatus.ERROR);
	    } 
	    
	    if (result.getStatus() != ResourceMapServiceStatus.ERROR && responseBody != "") {
	    	try { 
				//JSONObject json = new JSONObject(responseBody);
				JSONArray jsonArray = new JSONArray(responseBody);
				List<CollectionVO> collections = new LinkedList<CollectionVO>();
				for (int i=0; i<jsonArray.length(); i++) {
					JSONObject collectionJson = jsonArray.getJSONObject(i);
					CollectionVO collection = new CollectionVO();
					collection.setId(collectionJson.getInt("id"));
					collection.setDescription(collectionJson.getString("description"));
					collection.setName(collectionJson.getString("name"));
					collection.setPublic(collectionJson.getBoolean("public"));
					collections.add(collection);
				}
				
				result.setCollections(collections);
				result.setStatus(ResourceMapServiceStatus.SUCCESS);
				
			} catch (JSONException e) {
				result.setErrorMessage(e.getMessage());
		    	result.setStatus(ResourceMapServiceStatus.ERROR);
			}
	    }
	    return result;
	}
	
	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the timeOut
	 */
	public int getTimeOut() {
		return timeOut;
	}

	/**
	 * @param timeOut the timeOut to set
	 */
	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
	}
	
	public String getAuthToken() {
		return this.autToken;
	}
	
}
