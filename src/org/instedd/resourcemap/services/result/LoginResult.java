package org.instedd.resourcemap.services.result;

import org.instedd.resourcemap.services.ResourceMapServiceStatus;

public class LoginResult extends ResourceMapServiceStatus {
	private String authToken = null;
	public LoginResult() {
		
	}
	
	public String getAuthToken() {
		return this.authToken;
	}
	
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
}
