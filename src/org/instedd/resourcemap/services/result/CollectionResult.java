package org.instedd.resourcemap.services.result;

import java.util.LinkedList;
import java.util.List;

import org.instedd.resourcemap.services.ResourceMapServiceStatus;
import org.instedd.resourcemap.vo.CollectionVO;

public class CollectionResult extends ResourceMapServiceStatus {
	private List<CollectionVO> collections = new LinkedList<CollectionVO>();

	/**
	 * @return the collections
	 */
	public List<CollectionVO> getCollections() {
		return collections;
	}

	/**
	 * @param collections the collections to set
	 */
	public void setCollections(List<CollectionVO> collections) {
		this.collections = collections;
	}

	
}
