package org.instedd.resourcemap.tools;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class UrlConnection {
	public final static String resourceMapHost = "http://resmap-stg-ilab.instedd.org";
	public final static int connectionTimeOut = 15000; // 15 seconds
	public static boolean isServerAvailable() {
		boolean res = false;
		try {
			res = InetAddress.getByName(resourceMapHost).isReachable(connectionTimeOut);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
}
