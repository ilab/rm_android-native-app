package org.instedd.resourcemap;

import java.util.LinkedList;
import java.util.List;

import org.instedd.resourcemap.services.ResourceMapClient;
import org.instedd.resourcemap.services.ResourceMapServiceStatus;
import org.instedd.resourcemap.services.result.CollectionResult;
import org.instedd.resourcemap.services.result.LoginResult;
import org.instedd.resourcemap.vo.CollectionVO;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.support.v4.app.NavUtils;

public class CollectionActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ResourceMapClient client = ResourceMapClient.getInStance();
		CollectionResult collectionResult = client.getCollection();
		List<CollectionVO> collectionList = collectionResult.getCollections();
		
		setContentView(R.layout.activity_collection);
		// Show the Up button in the action bar.
		
		List<String> collections = new LinkedList<String>();
		for (int i=0; i<collectionList.size(); i++) {
			CollectionVO colElt= collectionList.get(i);
			collections.add(colElt.getName());
		} 
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.collection_element, collections);
		
		ListView listview = (ListView) findViewById(R.id.listview_collections);
		listview.setAdapter(adapter);
		setupActionBar();
		
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.collection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void logout(View view) {
		ResourceMapClient client = ResourceMapClient.getInStance();
        client.logout();
        Intent login = new Intent(CollectionActivity.this, LoginActivity.class);
    	this.startActivity(login);
	}

}
